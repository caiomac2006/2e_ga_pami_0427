import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  rotas = [
    {
      path: "/cad-autor",
      text: "Cadastrar Autor"
    },
    {
      path: "/cad-editora",
      text: "Cadastrar Editora"
    },
    {
      path: "/cad-livro",
      text: "Cadastrar Livro"
    }
  ]

  constructor() {}

}
