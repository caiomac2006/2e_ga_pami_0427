import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadAutorPageRoutingModule } from './cad-autor-routing.module';

import { CadAutorPage } from './cad-autor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadAutorPageRoutingModule
  ],
  declarations: [CadAutorPage]
})
export class CadAutorPageModule {}
