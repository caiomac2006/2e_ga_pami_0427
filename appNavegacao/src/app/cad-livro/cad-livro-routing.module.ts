import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadLivroPage } from './cad-livro.page';

const routes: Routes = [
  {
    path: '',
    component: CadLivroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadLivroPageRoutingModule {}
